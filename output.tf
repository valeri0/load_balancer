output "output_lb_target_group" {
  description = "dictionary of created target groups"
  value       = aws_lb_target_group.lb_targets
}
output "output_lb_listener" {
  description = "dictionary of created listeners"
  value       = aws_alb_listener.alb_listener
}
output "nlb" {
  description = "dictionary of created listeners"
  value       = var.nlb_name
}
output "var" {
  description = "dictionary of created listeners"
  value       = { nlb_name = var.nlb_name, redirect = local.redirect, expanded_target_group = local.expanded_target_group, nlb_name_is_not_null = var.nlb_name != null ? true : false }
}

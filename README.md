## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.4.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_alb_listener.alb_listener](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/alb_listener) | resource |
| [aws_alb_listener.nlb_listener](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/alb_listener) | resource |
| [aws_alb_listener_rule.route_path](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/alb_listener_rule) | resource |
| [aws_cloudwatch_metric_alarm.alb_healthyhosts](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_lb_target_group.lb_targets](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group) | resource |
| [aws_lb_target_group.nlb_targets](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group) | resource |
| [aws_lb_target_group_attachment.tg_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group_attachment) | resource |
| [aws_lb.lb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/lb) | data source |
| [aws_lb.nlb](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/lb) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_alarm_arn"></a> [alarm\_arn](#input\_alarm\_arn) | SNS topic to alert when balancer fails | `string` | n/a | yes |
| <a name="input_default_cname"></a> [default\_cname](#input\_default\_cname) | cname to use if not specified insied target\_groups | `string` | n/a | yes |
| <a name="input_deploy_environment"></a> [deploy\_environment](#input\_deploy\_environment) | test or prod environment | `string` | n/a | yes |
| <a name="input_deregistration_delay"></a> [deregistration\_delay](#input\_deregistration\_delay) | The amount time for Elastic Load Balancing to wait before changing the state of a deregistering target from draining to unused. | `number` | `300` | no |
| <a name="input_lb_name"></a> [lb\_name](#input\_lb\_name) | name of LB | `string` | n/a | yes |
| <a name="input_listener"></a> [listener](#input\_listener) | dictionary describing listener features: port, protocl, type, etc etc. | `map(any)` | n/a | yes |
| <a name="input_nlb_name"></a> [nlb\_name](#input\_nlb\_name) | name of LB | `string` | `null` | no |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | prefix name for infrastructure, ex. fdh, dpl, bitots | `string` | n/a | yes |
| <a name="input_repository_name"></a> [repository\_name](#input\_repository\_name) | name of repository, used to create alarm name | `string` | n/a | yes |
| <a name="input_ssl_certificate_arn"></a> [ssl\_certificate\_arn](#input\_ssl\_certificate\_arn) | ARN of the default SSL server certificate | `string` | n/a | yes |
| <a name="input_stickiness"></a> [stickiness](#input\_stickiness) | to allow route the requests for a particular sessions to the same container that serviced the first request for that session | `bool` | `false` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | a dictionary of the tag to add to resource | `map(any)` | `{}` | no |
| <a name="input_target_group"></a> [target\_group](#input\_target\_group) | one or more containers serving requests | `map(any)` | n/a | yes |
| <a name="input_target_group_prefix"></a> [target\_group\_prefix](#input\_target\_group\_prefix) | prefixed when naming target group resources | `string` | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | virtual private cloud identifier | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_nlb"></a> [nlb](#output\_nlb) | dictionary of created listeners |
| <a name="output_output_lb_listener"></a> [output\_lb\_listener](#output\_output\_lb\_listener) | dictionary of created listeners |
| <a name="output_output_lb_target_group"></a> [output\_lb\_target\_group](#output\_output\_lb\_target\_group) | dictionary of created target groups |
| <a name="output_var"></a> [var](#output\_var) | dictionary of created listeners |

data "aws_lb" "lb" {
  name = var.lb_name
}
data "aws_lb" "nlb" {
  count = var.nlb_name != null ? 1 : 0
  name  = var.nlb_name
}
resource "aws_alb_listener" "alb_listener" {
  for_each          = var.listener
  certificate_arn   = each.value["source_protocol"] != "HTTPS" ? null : var.ssl_certificate_arn
  load_balancer_arn = data.aws_lb.lb.arn
  port              = each.value["balancer_port"]
  protocol          = each.value["source_protocol"]
  ssl_policy        = each.value["source_protocol"] != "HTTPS" ? null : var.ssl_policy
  default_action {
    target_group_arn = each.value["type"] == "forward" ? aws_lb_target_group.lb_targets[each.value["default_action"]].arn : null
    type             = each.value["type"]
    dynamic "redirect" {
      for_each = each.value["type"] == "redirect" ? [1] : []
      content {
        port        = each.value["destination_port"]
        protocol    = each.value["destination_protocol"]
        status_code = "HTTP_301"
      }
    }
  }
}
resource "aws_lb_target_group" "nlb_targets" {
  for_each = var.nlb_name != null ? local.expanded_target_group : {}
  lifecycle {
    create_before_destroy = true
    ignore_changes        = [name]
  }
  name        = substr(format("%s-%s", "${var.prefix}-${var.deploy_environment}-${index(keys(local.expanded_target_group), each.key)}-ntg", replace(uuid(), "-", "")), 0, 32)
  port        = each.value["listener"]
  protocol    = "TCP"
  target_type = "alb"
  tags        = var.tags
  vpc_id      = var.vpc_id
}
resource "aws_lb_target_group_attachment" "tg_attachment" {
  for_each         = var.nlb_name != null ? local.expanded_target_group : {}
  depends_on       = [aws_alb_listener.alb_listener]
  port             = each.value["listener"]
  target_group_arn = aws_lb_target_group.nlb_targets[each.key].arn
  target_id        = data.aws_lb.lb.arn
}
resource "aws_lb_target_group" "lb_targets" {
  for_each             = var.target_group
  deregistration_delay = var.deregistration_delay
  lifecycle {
    create_before_destroy = true
    ignore_changes        = [name]
  }
  name        = substr(format("%s-%s", "${var.prefix}-${var.deploy_environment}-${index(keys(var.target_group), each.key)}-tg", replace(uuid(), "-", "")), 0, 32)
  port        = each.value["destination_port"]
  protocol    = each.value["destination_protocol"]
  target_type = lookup(each.value, "target_type", null) != null ? each.value["target_type"] : "ip"

  tags   = var.tags
  vpc_id = var.vpc_id
  dynamic "stickiness" {
    for_each = lookup(each.value, "destination_protocol", null) != "TCP" ? [1] : []
    content {
      type    = "lb_cookie"
      enabled = each.value["stickiness"]
    }
  }
  dynamic "health_check" {
    for_each = lookup(each.value, "health_path", null) != null ? [1] : []
    content {
      matcher             = each.value["health_http_code"]
      protocol            = each.value["health_protocol"]
      path                = each.value["health_path"]
      timeout             = lookup(each.value, "timeout", null)
      interval            = lookup(each.value, "interval", null)
      healthy_threshold   = lookup(each.value, "healthy_threshold", null)
      unhealthy_threshold = lookup(each.value, "unhealthy_threshold", null)
    }
  }
}
resource "aws_alb_listener_rule" "route_path" {
  for_each     = { for k, v in var.target_group : k => v if v["destination_protocol"] != "TCP" }
  listener_arn = aws_alb_listener.alb_listener[each.value["listener"]].arn
  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lb_targets[each.key].arn
  }
  condition {
    host_header {
      values = [
        lookup(each.value, "cname", "not present") == "not present" ? var.default_cname : each.value["cname"]
      ]
    }
  }
  lifecycle {
    ignore_changes = [priority]
  }
}
resource "aws_cloudwatch_metric_alarm" "alb_healthyhosts" {
  for_each = {
    for k, v in var.target_group :
    k => v
    if v["alarm_enabled"] == "true"
  }
  actions_enabled     = "true"
  alarm_actions       = [var.alarm_arn]
  alarm_description   = "Are node healthy?"
  alarm_name          = "${var.repository_name}-${var.deploy_environment}-balancer"
  comparison_operator = "LessThanThreshold"
  dimensions = {
    TargetGroup  = aws_lb_target_group.lb_targets["httpd"].arn_suffix
    LoadBalancer = data.aws_lb.lb.arn_suffix
  }
  evaluation_periods = "1"
  metric_name        = "HealthyHostCount"
  namespace          = "AWS/ApplicationELB"
  ok_actions         = [var.alarm_arn]
  period             = "60"
  statistic          = "Average"
  tags               = var.tags
  threshold          = 1
}
resource "aws_alb_listener" "nlb_listener" {
  for_each          = var.nlb_name != null ? var.listener : tomap({})
  load_balancer_arn = data.aws_lb.nlb[0].arn
  port              = each.value["balancer_port"]
  protocol          = "TCP"
  tags              = var.tags
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.nlb_targets[local.redirect && each.key == "80" ? "redirect" : each.value["default_action"]].arn
  }
}

variable "alarm_arn" {
  type        = string
  description = "SNS topic to alert when balancer fails"
}
variable "default_cname" {
  type        = string
  description = "cname to use if not specified insied target_groups"
}
variable "deploy_environment" {
  description = "test or prod environment"
  type        = string
}
variable "deregistration_delay" {
  default     = 300
  description = "The amount time for Elastic Load Balancing to wait before changing the state of a deregistering target from draining to unused."
  type        = number
}
variable "lb_name" {
  type        = string
  description = "name of LB"
}
variable "nlb_name" {
  default     = null
  type        = string
  description = "name of LB"
}
variable "prefix" {
  description = "prefix name for infrastructure, ex. fdh, dpl, bitots"
  type        = string
}
variable "listener" {
  type        = map(any)
  description = "dictionary describing listener features: port, protocl, type, etc etc."
}
variable "repository_name" {
  type        = string
  description = "name of repository, used to create alarm name"
}
variable "ssl_certificate_arn" {
  type        = string
  description = "ARN of the default SSL server certificate"
}
variable "tags" {
  default     = {}
  type        = map(any)
  description = "a dictionary of the tag to add to resource"
}
variable "ssl_policy" {
  type        = string
  description = "default on old one but you should use ELBSecurityPolicy-TLS13-1-2-2021-06"
  default     = "ELBSecurityPolicy-2016-08"
}
variable "target_group" {
  type        = map(any)
  description = "one or more containers serving requests"
}
variable "vpc_id" {
  type        = string
  description = "virtual private cloud identifier"
}
locals {
  redirect              = var.nlb_name != null && lookup(var.listener, "80", null) != null ? var.listener["80"]["type"] == "redirect" : false
  expanded_target_group = merge(var.target_group, local.redirect ? { "redirect" = { "listener" = "80" } } : tomap({}))
}
